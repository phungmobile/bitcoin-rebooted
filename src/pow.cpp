// Copyright (c) 2009-2016 The Bitcoin Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "pow.h"

#include "arith_uint256.h"
#include "chain.h"
#include "primitives/block.h"
#include "uint256.h"
#include "util.h"
#include "validation.h"

#include <cmath>
#include <string>
#include <time.h>

//
//  Fuzzy Effect Wave code by CCGLLC, September, 2018
//
//  Use of average difficulty adapted from and credit due to https://gist.github.com/GeertJohan/b28da8105babf0553f21
//
//  Statistical group sizes.  The larger group size one is, the more computationally intensive, but better statistics
//  Same holds for group two size, but the smaller group two, the faster anomolies are detected
//  However, too small of a group two size will result in excessive false positives with Effect Size overrides kicking in
//  Changing any of these defines requires a blockchain restart 
//
//  *** WARNING ***  Changing any of these #defines will require a blockchain restart.  Spend LOTS of time testing if you do.
//
//  GroupOneSize:  The number of blocks used to calculate the long term average and overall standard deviation
#define GROUPONESIZE 192 
//  GroupTwoSize:  The number of blocks used to calcuate the short term average and the 2nd factor in the Effect Size calculation
#define GROUPTWOSIZE 11
//  Range:  Responsibile factor for determining when long term averages are normal, and how much "overshoot" should be used when not.  Also caps maximium difficulty change.  Set to Euler's constant "e".
#define RANGE 2.718282
#define ONEOVERRANGE (1.0/RANGE)
//  Float_Factor:  Simply used to scale up/down integer math so that fractions < 1 are not lost to that integer math
#define FLOAT_FACTOR 16384
//  FirstCliff:  Multiplier against nTargetSpacing that defines when the cliff code starts, e.g. a value of 4 means 600 seconds
#define FIRSTCLIFF 6

//  Be very careful that global variables are reference only, not used in calculations or the multi-threaded nature of this system
//  WILL bite you in the ass.
//  Note, ALL globals should be placed here for easy reference and identification
double LastMean;   //  Global since its passed back for reporting when a block is found (reference only, not critical)
double LastPDA;  // Same as LastMean;
double Mean1Low, Mean1High;   // Set during first time processing
double Mean2Low, Mean2High;   // Set during first time processing
unsigned int nTargetSpacing;  // Set during first time processing
bool DAAlog=false;  // Set during first time processing
bool GNWRFirst=true; // Set to false during first time processing
static int GNWRLastnHeight; // Key, code only used if this is equal to previous call
static int DAAMaxHeight;  // Used to limit DAA diagnostic output
unsigned int GNWRLastDiff;
time_t GNWRLastBlockTime;


//  Note:  FuzzyEffectWave should ONLY be called from GetNextWorkRequired, since that function does some range checking
unsigned int FuzzyEffectWave(const CBlockIndex* pindexLast, const CBlockHeader *pblock, const Consensus::Params &params) {

    const arith_uint256 bnPowLimit = UintToArith256(params.powLimit);
    // Genesis & low blocks, skipping the first blocks as well since its block time will be huge
    // Note this is a double check, GetNextWorkRequired also performs this trivial test, but FuzzyEffectWave will
    // seg-fault if its not done, so best to be safe (say, if for some reason in the future, it is called directly)
    if ((pindexLast == NULL) || (pindexLast->nHeight < 2 ))
        return bnPowLimit.GetCompact();

    bool DAAlogit = DAAlog && (pindexLast->nHeight > DAAMaxHeight);

    const CBlockIndex *BlockReading;
    int64_t LastBlockTime = 0;
    int64_t TimeDiff[GROUPONESIZE+1];

    arith_uint256 PastDifficultySum;

    // First pass over the entire sample size
    int i,nBlocksCounted1 = 0;
    int64_t nActualTimespan1 = 0;
    int64_t LastTimeDiff;
    double Mean1;
    arith_uint256 PastDifficultyAverage1;

    BlockReading = pindexLast;
    LastBlockTime = pindexLast->GetBlockTime();
    for (i = 1; i<=GROUPONESIZE; i++) {
        nBlocksCounted1++;
        if (nBlocksCounted1 == 1) PastDifficultySum = arith_uint256().SetCompact(BlockReading->nBits);
	// 2nd through Nth iteration
        else {
	    PastDifficultySum += arith_uint256().SetCompact(BlockReading->nBits);
            LastTimeDiff = (LastBlockTime - BlockReading->GetBlockTime());
            nActualTimespan1 += LastTimeDiff;
	    TimeDiff[nBlocksCounted1] = LastTimeDiff;    // Save for standard deviation check
	}
        if ((BlockReading->nHeight == 1) || (BlockReading->pprev == NULL)) { 
	    assert(BlockReading);
	    break; 
	}  
        LastBlockTime = BlockReading->GetBlockTime();
        BlockReading = BlockReading->pprev;
    }
    int nTimesCounted1 = nBlocksCounted1 - 1;
    Mean1 = (double)nActualTimespan1 / (double)nTimesCounted1;
    PastDifficultyAverage1 = PastDifficultySum / (double)nBlocksCounted1;
    //  Record for reporting when a block is found
    LastMean = Mean1;   
    LastPDA = ConvDiff(PastDifficultyAverage1.GetCompact());

    // Second pass over the last few blocks
    int nBlocksCounted2 = 0;
    int64_t nActualTimespan2 = 0;
    // Limit any Mean2 block time to Cliff cutin (trigger 0 below)
    // This is a cheat to keep Mean2 from swinging too far and then getting destroyed when the block falls off the list
    // but its only temporary, since that falloff will happen pretty quickly (within GROUPTWOSIZE blocks)
    int64_t nMaxBlockTime = nTargetSpacing * RANGE;
    double Mean2;
    arith_uint256 PastDifficultyAverage2;

    BlockReading = pindexLast;
    LastBlockTime = pindexLast->GetBlockTime();
    for (i = 1; i<=GROUPTWOSIZE; i++) {
        nBlocksCounted2++;
        if (nBlocksCounted2 == 1) PastDifficultySum = arith_uint256().SetCompact(pindexLast->nBits);
        // 2nd through Nth iteration
	else {
	    int64_t nBlockTime = (LastBlockTime - BlockReading->GetBlockTime());
	    PastDifficultySum += arith_uint256().SetCompact(BlockReading->nBits);
	    //  Limit short term impact of cliffs so code ramps down a bit smoother.
	    if (nBlockTime > nMaxBlockTime) {
	        nBlockTime = nMaxBlockTime;
	    }
            nActualTimespan2 += nBlockTime;
        }
        if ((BlockReading->nHeight == 1) || (BlockReading->pprev == NULL)) { 
	    assert(BlockReading);
	    break; 
	}  
        LastBlockTime = BlockReading->GetBlockTime();
        BlockReading = BlockReading->pprev;
    }
    int nTimesCounted2 = nBlocksCounted2 - 1;
    Mean2 = (double)nActualTimespan2 / (double)nTimesCounted2;
    PastDifficultyAverage2 = PastDifficultySum / (double)nBlocksCounted2;

    // Standard deviation of blocktimes over entire sample size
    double SqMean,SqDiff,StdDev;
    BlockReading = pindexLast;
    SqDiff = 0;
    for (i = 2; i<=nBlocksCounted1; i++) {
        double delta = (double)TimeDiff[i] - Mean1;
        SqDiff += delta * delta;
    }
    //  Adjust divisor since we have N-1 time stats we are basing StdDev on.
    SqMean = SqDiff / (double)(nTimesCounted1);
    StdDev = std::sqrt(SqMean);

    double MeanDiff,EffectSize=0;
    //  Calculate the absolute value of the difference of means
    //  or... at least that is the straight forward math...
    //  however, we know our desired Mean1 = nTargetSpacing, so instead we drive towards that.
    //  if (Mean1 > Mean2) MeanDiff = Mean1 - Mean2;
    //  else MeanDiff = Mean2 - Mean1;
    double dTargetSpacing = nTargetSpacing;
    if (dTargetSpacing > Mean2) MeanDiff = dTargetSpacing - Mean2;
    else MeanDiff = Mean2 - dTargetSpacing;

    //  Finally calculate the Effect Size - the key to the Fuzzy Logic algorithm
    if (StdDev != 0) EffectSize = MeanDiff / StdDev;
    
    bool low1 = false;
    bool high1 = false;
    bool low2 = false;
    bool high2 = false;
    double Adjustment1,Adjustment2;
    double MeanAdj;
    std::string why1,why2;
    // Allow for speed to increase faster then decrease since surging do to zpool "attacks" is more likely a problem
    // than recoving from one compliments of the emergency difficulty adjustments
    // Note that 5 & 3 are pretty agressive
    //
    // This is also "human" logic, where bigger is harder, not "nBits" logic that is inverted.  Conversion is done at the last step.
    // If Target = 150, and Actual = 450, Adjustments will be 0.3333 (much harder)
    // If Target = 150, and Actual = 50, Adjustments will be 3 (much easier)
    // 
    
    // Calculate Group One adjustment
    Adjustment1 = nTargetSpacing / Mean1;
    MeanAdj = Mean1 - (nTargetSpacing - Mean1);  // Double the difference and recalculate
    if (MeanAdj < RANGE) MeanAdj = RANGE;
    Adjustment1 = nTargetSpacing / MeanAdj;
    if (Mean1 < Mean1Low) { 
        // Need to increase the difficulty
        why1 = "Mean1 Low";
	low1 = true;
        Adjustment1 *= RANGE;
    }
    else if (Mean1 > Mean1High) { // must be high
         // Need to decrease the difficulty
         why1 = "Mean1 High";
	 high1= true;
         Adjustment1 *= ONEOVERRANGE;  // multiplication is faster computationally (compiler does division, run time multiplication)
    }
    else why1 = "Mean1 OK";

    // Calculate Group Two adjustment
    // Note Group Two adjustment is over a wider range of value vs. Group One
    // Go with straight math, don't double like Adjustment1 as its already short term and responsive
    Adjustment2 = nTargetSpacing/Mean2;
  
    if (Mean2 < Mean2Low) {  // Secondary check, same response as Mean1 Low
       why2 = "Mean2 Low";
       low2 = true;
       Adjustment2 *= RANGE;  
    }
    else if (Mean2 > Mean2High) { 
         // Need to decrease the difficulty
         why2 = "Mean2 High";
	 high2= true;
         Adjustment2 *= ONEOVERRANGE;  // multiplication is faster computationally (compiler does division, run time multiplication)
    }
    else why2 = "Mean2 OK";

    //
    // Now make sure Mean2 isn't working in conflict with Mean1
    //
    if ((low1 && high2) || (low2 && high1)) {
       // Override and let Mean1 completely drive the calculation
       EffectSize = 0;
       low2 = false;
       high2 = false;
       why2 = "*Nulled*";
    }

    //
    // Apply the fuzzy logic - scale between the two sets of data based on Effect Size
    //
    // This gets a bit tricky since all arith_uint256 math must be done against int64_t type values
    // Therefore extra variables will be used to ensure proper type casting and FLOAT_FACTOR
    // will be applied to scale up for fractional math
    //
    // In particular, we need to work around that there is no way to sum two arith_uint256 values
    //
    if (EffectSize > 1) EffectSize = 1;
    double Adj1 = (1.0 - EffectSize) * Adjustment1;
    double Adj2 = EffectSize * Adjustment2;
    double cdAdjPDA1 = Adj1 * ConvDiff(PastDifficultyAverage1.GetCompact());
    double cdAdjPDA2 = Adj2 * ConvDiff(PastDifficultyAverage2.GetCompact());
    // Ideally, we want bnNew = (Adj1 * PDA1) + (Adj2 * PDA2), so...
    double cdNew = cdAdjPDA1 + cdAdjPDA2;   // This is the blended new difficulty in human readable terms (like getmininginfo displays)
    double cdOld = ConvDiff(PastDifficultyAverage1.GetCompact());  // And this is the human readable long term average
    assert(cdOld);  // Die cleanly if we are about to zero-divide - SHOULD NEVER HAPPEN!!!
    // 
    // *** Remember that nBits difficulty is half-ass backwards, so Adustments < 1 make it harder and > 1 make it easier ***
    //     (and that ConvDiff(nBits) inverts that for human readability)
    //
    // Normal case - calculate over GROUPONESIZE data points
    // If Target = 150, and Actual = 50, cdFactor will be 0.3333 (much harder)
    // If Target = 150, and Actual = 450, cdFactor will be 3 (much easier)
    //
    // In order to do this, take the reciprical of the human readable value (or actually just do B/A instead of A/B)
    double cdFactor = (cdOld / cdNew);  // Math is inverted like binary difficulty. 
    arith_uint256 bnNew = PastDifficultyAverage1;

    bnNew *= (int64_t)(cdFactor * (double)FLOAT_FACTOR);
    bnNew /= FLOAT_FACTOR;
    // And there we should have it!

    arith_uint256 bnLastBlock = arith_uint256().SetCompact(pindexLast->nBits);

    //
    // Sanity checks.  Remember that bigger is easier, smaller is harder
    // So if Low, the new difficulty should be smaller than the previous block difficulty
    // Likewise, if High, the new difficulty should be bigger than the previous block difficulty
    //
    // Override logic is simple:  If the previous difficulty managed to find a block, its a workable diff.  If it
    //     is more severe than what is calculated, use it instead so we reach our goal faster.  e.g.  If the average
    //     is low, and the previous block was found with a harder difficulty than we calculated, use the harder difficulty
    //
    double cdDiffOld = ConvDiff(bnLastBlock.GetCompact());
    double cdDiffNew = ConvDiff(bnNew.GetCompact());
    double cdMaxNew = cdDiffOld * RANGE;
    double cdMinNew = cdDiffOld * ONEOVERRANGE;
    int64_t nffRange = (double)(RANGE * FLOAT_FACTOR);
    // Limit overall rate of change of the difficulty 
    if (cdDiffNew > cdMaxNew) {
        bnNew = bnLastBlock;
	bnNew /= nffRange;
	bnNew *= FLOAT_FACTOR;
	if (DAAlogit) {
	   LogPrintf("DAA(%d):  **Limited Diff Rise** Old diff %f, New diff %f limited to %f\n",
	      pindexLast->nHeight, cdDiffOld, cdDiffNew, ConvDiff(bnNew.GetCompact()));
	}
    }
    else if (cdDiffNew < cdMinNew) {
        bnNew = bnLastBlock;
        bnNew *= nffRange;
        bnNew /= FLOAT_FACTOR;
	if (DAAlogit) {
	   LogPrintf("DAA(%d):  **Limited Diff Fall** Old diff %f, New diff %f limited to %f\n",
	      pindexLast->nHeight, cdDiffOld, cdDiffNew, ConvDiff(bnNew.GetCompact()));
	}
    }
    // If calculated difficulty is lower than the minimal diff, set the new difficulty to be the minimal diff.
    int64_t nLastBlockTime = pindexLast->GetBlockTime() - pindexLast->pprev->GetBlockTime();
    if (bnNew > bnPowLimit){
        if (DAAlogit)
	   LogPrintf("DAA(%d):  **Override** New diff of %f below minimum of %f, setting to minimum.\n",
	      pindexLast->nHeight,ConvDiff(bnNew.GetCompact()),cdDiffOld);
        bnNew = bnPowLimit;
    }
    // If an average is low, the the last block times was somewhat reasonable, use the harder of the calcuated and last block diffs
    // Note this blcok may have been found after a cliff adjustment, so be very generous on what is considered reasonable
    if ((low1 || low2) && !high1 && (nLastBlockTime <= (nTargetSpacing*RANGE)) && !(bnNew < bnLastBlock)) {
        if (DAAlogit)
            LogPrintf("DAA(%d):  **Override** Mean1 and/or 2 is low but new diff of %f is less than previous %f so setting new diff to previous.\n", 
	       pindexLast->nHeight,ConvDiff(bnNew.GetCompact()),cdDiffOld);
        bnNew = bnLastBlock;
    }
    // If an average is high, the the last block times was somewhat reasonable, use the easier of the calcuated and last block diffs
    else if ((high1 || high2) && !low1 && (nLastBlockTime >= (nTargetSpacing*ONEOVERRANGE)) && !(bnNew > bnLastBlock)) {
        if (DAAlogit)
            LogPrintf("DAA(%d):  **Override** Mean1 and/or 2 is high but new diff of %f is greater than previous %f so setting new diff to previous.\n", 
	        pindexLast->nHeight,ConvDiff(bnNew.GetCompact()),cdDiffOld);
        bnNew = bnLastBlock;
    }
    // End of Sanity Checks.  

    // Return the new diff.
    if (DAAlogit) {
        assert(cdFactor);  // Die cleanly if we are about to zero-divide - SHOULD NEVER HAPPEN!!!
        LogPrintf("DAA(%d):  %-10s %5.1f, %-10s %5.1f, Blocktime: %3d, Effectors: PDA1 %f %5.1f%% - PDA2 %f %5.1f%%, PDA1 multiplier: %.4f, Last diff: %f, Next diff: %f\n", 
	    pindexLast->nHeight, why1, Mean1, why2, Mean2, nLastBlockTime, 
	    ConvDiff(PastDifficultyAverage1.GetCompact()), (1.0-EffectSize)*100,
	    ConvDiff(PastDifficultyAverage2.GetCompact()), EffectSize*100,
	    1.0/cdFactor,cdDiffOld,ConvDiff(bnNew.GetCompact()));
	DAAMaxHeight = pindexLast->nHeight;
    }
    return bnNew.GetCompact();
}

// Code pulled out of mainline for efficiency - only called if there are repeated getblocktemplate calls without a found block
// Note this is the cliff breaking code, validation code is the next routine down
unsigned int GNWRCheckCliff(const CBlockIndex* pindexLast, const Consensus::Params& params) {

    if (IsInitialBlockDownload()) {
       LogPrintf("Doing initial block download...\n");
       return false;
    }
    const arith_uint256 bnPowLimit = UintToArith256(params.powLimit);
    int timepast = time(NULL) - GNWRLastBlockTime;
    int trigger0 = params.nPowTargetSpacing*FIRSTCLIFF;
    int trigger1 = params.nPowTargetSpacing*FIRSTCLIFF*3;
    int trigger2 = trigger1 + trigger1;
    int trigger3 = trigger2 + trigger2;
    bool triggered = false;
    arith_uint256 bnNew;
    if (timepast >= trigger3) {
        triggered = true;
        bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("DAA(%d):  Last block found %d seconds ago.  Emergency 'Cliff Hung' MIN diff %f being offered to miners ***\n",
            pindexLast->nHeight,timepast,ConvDiff(bnNew.GetCompact()));
    }
    else if (timepast >= trigger2) {
        triggered = true;
        bnNew.SetCompact(GNWRLastDiff);
        bnNew *= 64;
        if (bnNew > bnPowLimit) bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("DAA(%d):  Last block found %d secconds ago.  Emergency ~2%% 'Cliff Hanging' diff %f being offered to miners ***\n",
            pindexLast->nHeight,timepast,ConvDiff(bnNew.GetCompact()));
    }
    else if (timepast >= trigger1) {
        triggered = true;
        bnNew.SetCompact(GNWRLastDiff);
        bnNew *= 8;
        if (bnNew > bnPowLimit) bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("DAA(%d):  Last block found %d seconds ago.  Emergency 12.5%% 'Cliff Hanging' diff %f being offered to miners ***\n", 
            pindexLast->nHeight,timepast,ConvDiff(bnNew.GetCompact()));
    }
    else if (timepast >= trigger0) {
        triggered = true;
        bnNew.SetCompact(GNWRLastDiff);
        bnNew *= 2;
        if (bnNew > bnPowLimit) bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("DAA(%d):  Last block found %d seconds ago.  50%% adjustment diff %f being offered to miners ***\n", 
            pindexLast->nHeight,timepast,ConvDiff(bnNew.GetCompact()));
    }
    if (triggered) {  // Now do everything in ccmmon for the 3 above trigger situations
        GNWRFirst = false;
        GNWRLastnHeight = pindexLast->nHeight;
        GNWRLastBlockTime = pindexLast->GetBlockTime();
	return bnNew.GetCompact();
    }
    return 0;
}

unsigned int GetNextWorkRequired(const CBlockIndex* pindexLast, const CBlockHeader *pblock, const Consensus::Params& params, const bool cliffcheck)
{
    const arith_uint256 bnPowLimit = UintToArith256(params.powLimit);

    // Avoid logic complications below
    if ((pindexLast == NULL) || (pindexLast->nHeight < 2 ))
        return bnPowLimit.GetCompact();


    // Prevent difficulty being based on time since code was last launched
    if (GNWRFirst) {  
       nTargetSpacing = params.nPowTargetSpacing;
       double Offset = ((RANGE * nTargetSpacing) / (double)GROUPONESIZE);  
       Mean1Low = (double)nTargetSpacing - Offset;
       Mean1High = (double)nTargetSpacing + Offset;
       Mean2Low = (double)nTargetSpacing - (Offset * (double)GROUPONESIZE / (double)GROUPTWOSIZE);
       Mean2High = (double)nTargetSpacing + (Offset * (double)GROUPONESIZE / (double)GROUPTWOSIZE);
       DAAlog = IsArgSet("-daalog");
       if (DAAlog) {
           LogPrintf("DAA(Init):  Extra Difficulty Adjustment Algorithm (DAA) tracking enabled via -daalog option\n");
	   LogPrintf("DAA(Init):  Blocks in Group One: %d, Blocks in Group Two: %d\n",GROUPONESIZE,GROUPTWOSIZE);
	   LogPrintf("DAA(Init):  Group 1 Mean Target (Mean1): %d.  Typical range is between %f and %f\n",nTargetSpacing,Mean1Low,Mean1High);
	   LogPrintf("DAA(Init):  Group 2 Mean Target (Mean2): %d.  Typical range is between %f and %f\n",nTargetSpacing,Mean2Low,Mean2High);
	   LogPrintf("DAA(Init):  PDA[1,2] are the Previous Difficult Averages (PDA) for all blocks in Group 1 and 2\n\n");
       }
       GNWRLastnHeight = pindexLast->nHeight;
       GNWRLastDiff = pindexLast->nBits;
       GNWRLastBlockTime = pindexLast->GetBlockTime();
       DAAMaxHeight = pindexLast->nHeight;
    }

    //  Special cliff code - pass back to "getblocktemplate" lower difficulty if we have been hung awhile
    if (cliffcheck && (pindexLast->nHeight == GNWRLastnHeight)) {
       unsigned int CliffDiff = GNWRCheckCliff(pindexLast, params);
       if (CliffDiff) {
           return CliffDiff;
       }
       if (GNWRFirst) {  // Do this here mostly for the "else" below
           GNWRFirst = false;
       } else {
           return GNWRLastDiff;
       }
    }
    else GNWRFirst = false;  // Needed to keep from doing GNWRFirst processing for non-mining nodes
//    else if (DAAlog) LogPrintf("DAA(%d): Cliff not checked for since pindexLast->nHeight does not match GNWRLastnHeight of %d\n", 
//   	pindexLast->nHeight, GNWRLastnHeight);
    
    // Time savers for next call... maybe...
    GNWRLastDiff = FuzzyEffectWave(pindexLast, pblock, params);
    GNWRLastnHeight = pindexLast->nHeight;  //  Never set height before diff

    GNWRLastBlockTime = pindexLast->GetBlockTime();

    return GNWRLastDiff;

}

// Check if passed diff was historically possible
// Always verify logic against above routine
// Note this routine is only called from validation.cpp

bool GetNextWorkRequiredValidDiff(const CBlockIndex* pindexLast, const CBlockHeader *pblock, const Consensus::Params& params) 
    {
    unsigned int diff = pblock->nBits;
    // Save some time if possible vs. calling FuzzyEffectWave directly, also initializes DAAlog and other variables
    unsigned int dgw = GetNextWorkRequired(pindexLast, pblock, params, false);
    // Normal case first:
    if (diff == dgw) {
       return true;
    }
//    else if (DAAlog) LogPrintf("VDA(%d):  Failed DGW check, looking at history next\n",pindexLast->nHeight);
    if (pindexLast->nHeight <= 2) {
//       if (DAAlog) LogPrintf("VDA(%d):  First blocks, let them pass...\n",pindexLast->nHeight);
       return true;
    }
    int timepast = pblock->GetBlockTime() - pindexLast->GetBlockTime();
    int trigger0 = params.nPowTargetSpacing*FIRSTCLIFF;
    int trigger1 = params.nPowTargetSpacing*FIRSTCLIFF*3;
    int trigger2 = trigger1 + trigger1;
    int trigger3 = trigger2 + trigger2;
    if (DAAlog) LogPrintf("VDA(%d):  Time between blocks was %d seconds. t0: %d, t1: %d, t2:%d, t3:%d, validating diff %f, DGW base %f\n",
        pindexLast->nHeight,timepast,trigger0,trigger1,trigger2,trigger3,ConvDiff(diff),ConvDiff(dgw));
    arith_uint256 bnNew;
    const arith_uint256 bnPowLimit = UintToArith256(params.powLimit);
    if (timepast >= trigger3) {
        bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("VDA(%d):  Emergency 'Cliff Hung' MIN diff had been offered to miners -",pindexLast->nHeight);
	if (diff == bnNew.GetCompact()) {
	    if (DAAlog) LogPrintf(" matched!\n");
	    return true;
	}
        if (DAAlog) LogPrintf(" failed to match min diff %f\n", ConvDiff(bnNew.GetCompact()));
    }
    if (timepast >= trigger2) {
        //  Use dgw value instead of pindexLast->nBits since their may be adjustments
        bnNew.SetCompact(dgw);
        bnNew *= 64;
        if (bnNew > bnPowLimit) bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("VDA(%d):  Emergency ~2%% 'Cliff Hanging' diff had been offered to miners -",pindexLast->nHeight);
	if (diff == bnNew.GetCompact()) {
	    if (DAAlog) LogPrintf(" matched!\n");
	    return true;
	}
        if (DAAlog) LogPrintf(" %f failed to match %f\n", ConvDiff(diff),ConvDiff(bnNew.GetCompact()));
    }
    if (timepast >= trigger1) {
        //  Use dgw value instead of pindexLast->nBits since their may be adjustments
        bnNew.SetCompact(dgw);
        bnNew *= 8;
        if (bnNew > bnPowLimit) bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("VDA(%d):  Emergency 12.5%% 'Cliff Hanging' diff had been offered to miners -",pindexLast->nHeight);
	if (diff == bnNew.GetCompact()) {
	    if (DAAlog) LogPrintf(" matched!\n");
	    return true;
	}
        if (DAAlog) LogPrintf(" %f failed to match %f\n", ConvDiff(diff),ConvDiff(bnNew.GetCompact()));
    }
    if (timepast >= trigger0) {
        //  Use dgw value instead of pindexLast->nBits since their may be adjustments
        bnNew.SetCompact(dgw);
        bnNew *= 2;
        if (bnNew > bnPowLimit) bnNew = bnPowLimit;
        if (DAAlog) LogPrintf("VDA(%d):  50%% adjustment diff had been offered to miners -",pindexLast->nHeight);
	if (diff == bnNew.GetCompact()) {
	    if (DAAlog) LogPrintf(" matched!\n");
	    return true;
	}
        if (DAAlog) LogPrintf(" %f failed to match %f\n", ConvDiff(diff),ConvDiff(bnNew.GetCompact()));
    }
    return false;
}


unsigned int GetLastDiff() { return GNWRLastDiff; }

// Only used in validation.cpp as part of the Height LogPrintf()
double GetLastMean() { return LastMean ; }   
double GetLastPDA() { return LastPDA; }

bool CheckProofOfWork(uint256 hash, unsigned int nBits, const Consensus::Params& params)
{
    bool fNegative;
    bool fOverflow;
    arith_uint256 bnTarget;

    bnTarget.SetCompact(nBits, &fNegative, &fOverflow);

    // Check range
    if (fNegative || bnTarget == 0 || fOverflow || bnTarget > UintToArith256(params.powLimit))
        return false;

    // Check proof of work matches claimed amount
    if (UintToArith256(hash) > bnTarget)
        return false;

    return true;
}
