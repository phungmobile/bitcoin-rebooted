Bitcoin Core integration/staging tree
=====================================

[![Build Status](https://travis-ci.org/bitcoin/bitcoin.svg?branch=master)](https://travis-ci.org/bitcoin/bitcoin)

https://bitcoin-rebooted.xyz

What is Bitcoin?
----------------

Bitcoin is an experimental digital currency that enables instant payments to
anyone, anywhere in the world. Bitcoin uses peer-to-peer technology to operate
with no central authority: managing transactions and issuing money are carried
out collectively by the network. Bitcoin Core is the name of open source
software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the Bitcoin Core software, see https://bitcoin.org/en/download, or read the
[original whitepaper](https://bitcoincore.org/bitcoin.pdf).

What is Bitcoin Rebooted?
----------------

Bitcoin Rebooted is a restart of the 2009 Bitcoin blockchain in mid-2018 allowing 
the now known technology to more fairly distribute rewards to the current generation 
of cryptocurrency advocates.

Bitcoin Rebooted is based on the 0.14.2 release of Bitcoin Core - with all Segwit code 
removed.  This was simply faster than starting at 0.12.1 (the last Segwit free version) 
and porting forward all the performance improvements like BIP152's compact blocks 
(critical for >1MB block sizes).  Many other BIPs have been streamlined in the code
since they have been active since block #1.

Like Litecoin, Bitcoin Rebooted has a 2.5 minute target block time.

Unlike Bitcoin, Bitcoin Cash, or Litecoin, Bitcoin Rebooted has an annual reward 
adjustment that starts at 50 coins/block and is not halved every 4 years, but rather 
simply decrements by 1 coin per year.  

Documentation Note
----------------

Bitcoin Rebooted is a port from Bitcoin 0.14.2, with enhancements and additional security
patches.  Initial development effort was focused on functionality.  As such, many original
references to bitcoin will be found throughout this and other various documentation files.

License
-------

Bitcoin Rebooted is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.

Development Process
-------------------

The `master` branch is regularly built and tested, but is not guaranteed to be
completely stable. [Tags](https://bitbucket.org/ccgllc/Bitcoin-Rebotted/tags) are created
regularly to indicate new official, stable release versions of Bitcoin Core.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).

The developer [discord channel](*By invitation only, contact ccgllc on discord*)
should be used to discuss complicated or controversial changes before working
on a patch set.

Developer IRC can be found on Freenode at #bitcoin-core-dev.

Testing
-------

Testing and code review is the bottleneck for development; we get more pull
requests than we can review and test on short notice. Please be patient and help out by testing
other people's pull requests, and remember this is a security-critical project where any mistake might cost people
lots of money.

### Automated Testing

Developers are strongly encouraged to write [unit tests](src/test/README.md) for new code, and to
submit new unit tests for old code. Unit tests can be compiled and run
(assuming they weren't disabled in configure) with: `make check`. Further details on running
and extending unit tests can be found in [/src/test/README.md](/src/test/README.md).

There are also [regression and integration tests](/test), written
in Python, that are run automatically on the build server.
These tests can be run (if the [test dependencies](/test) are installed) with: `test/functional/test_runner.py`

The Travis CI system makes sure that every pull request is built for Windows, Linux, and OS X, and that unit/sanity tests are run automatically.

### Manual Quality Assurance (QA) Testing

Changes should be tested by somebody other than the developer who wrote the
code. This is especially important for large or high-risk changes. It is useful
to add a test plan to the pull request description if testing the changes is
not straightforward.

Translations
------------

Changes to translations as well as new translations can be submitted to
[Bitcoin Core's Transifex page](https://www.transifex.com/projects/p/bitcoin/).

Translations are periodically pulled from Transifex and merged into the git repository. See the
[translation process](doc/translation_process.md) for details on how this works.

**Important**: We do not accept translation changes as GitHub pull requests because the next
pull from Transifex would automatically overwrite them again.

Translators should also subscribe to the [mailing list](https://groups.google.com/forum/#!forum/bitcoin-translators).
